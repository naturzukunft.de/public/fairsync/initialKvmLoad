package de.naturzukunft.rdf.kvm.initialload;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.http.HTTPRepository;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class KVMToRDF4J {

//	private long entries = 0;
	private KvmMapper kvmMapper;
	
	public KVMToRDF4J() {
		kvmMapper = new KvmMapper();
	}
	
	public static void main(String[] args) {
		KVMToRDF4J kvmEntryToRepo = new KVMToRDF4J();
		kvmEntryToRepo.process();
	}

	
	private static Repository getRepository() {
		String rdf4jServer = "http://localhost:8080/rdf4j-server/";
		String repositoryID = "fairsync";
		Repository repo = new HTTPRepository(rdf4jServer, repositoryID);
		return repo;
	}

	private void process() {
		Set<String> entryIds = new ParseKVM().getAllVisibleKvmEntries();
		log.info("got " + entryIds.size() + " entries from kvm");
		List<List<String>> listOfList =  split(entryIds);
		try(RepositoryConnection con = getRepository().getConnection()) {
			listOfList.stream().forEach(list-> {
				Model listModel = new ModelBuilder().build();
				getEntries(list).forEach(entry->{
					listModel.addAll(kvmMapper.toEntry(entry));
				});					
				con.add(listModel);
			});
		}
	}

	/**
	 * Split all entryIds into small pieces to call the complete entries with the openFairDB API.
	 * @param entryIdsSet
	 * @return
	 */
	private static List<List<String>> split(Set<String> entryIdsSet) {
		List<String> entryIds = new ArrayList<>(entryIdsSet);
	    int sizeOfPart = 90;
	    List<List<String>> listOfList = new ArrayList<>();
	    while( entryIds.size() > sizeOfPart ) {
	    	List<String> part = new ArrayList<>();
	    	for (int i = 0; i < sizeOfPart; i++) {
				part.add(entryIds.remove(0));
			}
	    	listOfList.add(part);
	    }
	    listOfList.add(entryIds);
	    return listOfList;
	}

	private static List<org.openapitools.client.model.Entry> getEntries(List<String> entryIds) {
		RestTemplate t = new RestTemplate();
		String joinedIds = String.join(",", entryIds);
		String url = "https://api.ofdb.io/v0/entries/" + joinedIds;
		org.openapitools.client.model.Entry[] entry = t.getForObject(url, org.openapitools.client.model.Entry [].class);
		return Arrays.asList(entry);
	}
}
