package de.naturzukunft.rdf.kvm.initialload;


import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleNamespace;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.DCTERMS;
import org.eclipse.rdf4j.model.vocabulary.FOAF;

public class PERSON {

	/**
	 * The Person namespace: http://www.w3.org/ns/person#
	 */
	public static final String NAMESPACE = "http://www.w3.org/ns/person#";

	/**
	 * The recommended prefix for the Person namespace: "person"
	 */
	public static final String PREFIX = "person";

	/**
	 * An immutable {@link Namespace} constant that represents the Person namespace.
	 */
	public static final Namespace NS = new SimpleNamespace(PREFIX, NAMESPACE);

	// ----- Classes ------
	public final static IRI PERSON;

	// ----- Properties ------
	public final static IRI BIRTH_NAME;
	public final static IRI ALTERNATIVE_NAME;
	public final static IRI CITIZEN_SHIP;
	public final static IRI COUNTRY_OF_BIRTH;
	public final static IRI COUNTRY_OF_DEATH;
	public final static IRI DATE_OF_BIRTH;
	public final static IRI DATE_OF_DEATH;
	public final static IRI FAMILIY_NAME;
	public final static IRI FULL_NAME;
	public final static IRI GENDER;
	public final static IRI GIVEN_NAME;
	public final static IRI IDENTIFIER;
	public final static IRI PATRONYMIC_NAME;
	public final static IRI PLACE_OF_BIRTH;
	public final static IRI PLACE_OF_DEATH;
	public final static IRI RESIDENCY;
	
	static {
		ValueFactory factory = SimpleValueFactory.getInstance();

		// ----- Classes ------
		PERSON = factory.createIRI(NAMESPACE, "Person");

		// ----- Properties ------
		BIRTH_NAME = factory.createIRI(NAMESPACE, "birthName");
		ALTERNATIVE_NAME = DCTERMS.ALTERNATIVE;
		CITIZEN_SHIP = factory.createIRI(NAMESPACE, "citizenship");
		COUNTRY_OF_BIRTH = factory.createIRI(NAMESPACE, "countryOfBirth");
		COUNTRY_OF_DEATH = factory.createIRI(NAMESPACE, "countryOfDeath");
		DATE_OF_BIRTH = factory.createIRI("http://schema.org/birthDate");
		DATE_OF_DEATH = factory.createIRI("http://schema.org/deathDate");
		FAMILIY_NAME = FOAF.FAMILY_NAME;
		FULL_NAME = FOAF.NAME;
		GENDER = factory.createIRI("http://schema.org/gender");
		GIVEN_NAME = FOAF.GIVEN_NAME;
		IDENTIFIER = DCTERMS.IDENTIFIER;
		PATRONYMIC_NAME = factory.createIRI(NAMESPACE, "patronymicName");
		PLACE_OF_BIRTH = factory.createIRI(NAMESPACE, "placeOfBirth");
		PLACE_OF_DEATH = factory.createIRI(NAMESPACE, "placeOfDeath");
		RESIDENCY = factory.createIRI(NAMESPACE, "placeresidency");
	}
}
