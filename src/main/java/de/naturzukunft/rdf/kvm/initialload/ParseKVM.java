package de.naturzukunft.rdf.kvm.initialload;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.openapitools.client.model.SearchResponse;
import org.springframework.web.client.RestTemplate;

public class ParseKVM {

	public static void main(String[] args) {
		ParseKVM p = new ParseKVM();
		p.getAllVisibleKvmEntries();
	}
	
	public Set<String> getAllVisibleKvmEntries() {

		RestTemplate restTemplate = new RestTemplate();

		String urlPrefix = "https://api.ofdb.io/v0/search?bbox=";
		String urlPostfix = "&limit=500";
		double west = 4.0000000;
		double east = 17.000000;
		double north = 57.000000;
		double south = 45.074999999999996;
		double increase = 0.025;

		Set<String> allVisibleKvmEntries = new TreeSet();
		while (south < north) {
			String bbox = south + "," + west + "," + (south + increase) + "," + east;
			String url = urlPrefix + bbox + urlPostfix;
			SearchResponse res = restTemplate.getForObject(url, SearchResponse.class);
			long entries = res.getVisible().size();
			if (entries > 490) {
				throw new RuntimeException("bbox: " + bbox + " -> " + entries);
			}
			allVisibleKvmEntries.addAll(res.getVisible().stream().map(it-> it.getId()).collect(Collectors.toList()));
			south = south + increase;
		}
		return allVisibleKvmEntries;
	}

}
