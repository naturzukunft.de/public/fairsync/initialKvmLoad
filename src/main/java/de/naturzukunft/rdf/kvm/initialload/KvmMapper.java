package de.naturzukunft.rdf.kvm.initialload;

import static org.eclipse.rdf4j.model.util.Values.iri;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.impl.SimpleNamespace;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.model.vocabulary.DCTERMS;
import org.eclipse.rdf4j.model.vocabulary.FOAF;
import org.eclipse.rdf4j.model.vocabulary.LOCN;
import org.eclipse.rdf4j.model.vocabulary.OWL;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.model.vocabulary.RDFS;

import com.github.slugify.Slugify;

public class KvmMapper {

	public static String NAMESPACE = "http://fairsync.naturzukunft.de/";
	public static Namespace NS = new SimpleNamespace("fairsync", NAMESPACE);

	private static List<Namespace> namespaces = new ArrayList<>();
	private static final String BASE_URL = "http://fairsync.naturzukunft.de/";
	
	static {
		namespaces.add(NS);
		namespaces.add(PERSON.NS);
		namespaces.add(FOAF.NS);
		namespaces.add(RDFS.NS);
		namespaces.add(RDF.NS);
	}

	public Model toEntry(org.openapitools.client.model.Entry entry) {
		
		Model result = new ModelBuilder().build();
		String entryIriAsString = createEntryIri(entry);
		
		IRI geometryIRI = createRelatedIri(entryIriAsString, "geometry");
		result.addAll(createGeometryModel(entry, geometryIRI));
		
		IRI addressIRI = createRelatedIri(entryIriAsString, "address");
		result.addAll(createAddressModel(entry, addressIRI));
					
		IRI locationIRI = createRelatedIri(entryIriAsString, "location");
		result.addAll(createLocationModel(geometryIRI, addressIRI, locationIRI));
					
		IRI contactPointIRI = createRelatedIri(entryIriAsString, "contactPoint");
		result.addAll(createContactPointModel(entry, contactPointIRI));
		
		IRI entryIRI = iri(BASE_URL + entryIriAsString);
		ModelBuilder mb = createModelBuilder().subject(entryIRI).add(RDF.TYPE, FOAF.GROUP);
		
		mb.add(DCTERMS.LOCATION,locationIRI);
		mb.add(SCHEMA.CONTACT_POINT,contactPointIRI);
		
		Optional.ofNullable(entry.getId()).ifPresent(val->mb.add(PERSON.IDENTIFIER,val));
		Optional.ofNullable(entry.getTitle()).ifPresent(val-> mb.add(FOAF.NAME, val));
		Optional.ofNullable(entry.getHomepage()).ifPresent(val->mb.add(FOAF.HOMEPAGE, val));
//	"created": 1591255220,
		Optional.ofNullable(entry.getVersion()).ifPresent(val->mb.add(OWL.VERSIONINFO, val));
		Optional.ofNullable(entry.getDescription()).ifPresent(val->mb.add(DCTERMS.DESCRIPTION, val ));
//	"opening_hours": null,
		Optional.ofNullable(entry.getFoundedOn()).ifPresent(val->mb.add(SCHEMA.FOUNDED_DATE, val ));
		result.addAll(mb.build());
		return result;
	}

	private static String createEntryIri(org.openapitools.client.model.Entry entry) {
		Optional<String> iriIdOptional = Optional.ofNullable( entry.getId() );
		
		String entryIri = iriIdOptional.map(iriId -> new Slugify().slugify(iriId)).orElseThrow(()->new RuntimeException("no id"));
		return entryIri;
	}

	private Model createContactPointModel(org.openapitools.client.model.Entry entry,
			IRI contactPointIRI) {
		ModelBuilder contactPointBuilder = createModelBuilder().subject(contactPointIRI).add(RDF.TYPE, SCHEMA.CONTACT_POINT);
		Optional.ofNullable(entry.getContactName()).ifPresent(val->contactPointBuilder.add(SCHEMA.NAME,val));
		Optional.ofNullable(entry.getEmail()).ifPresent(val->contactPointBuilder.add(SCHEMA.EMAIL,val));
		Optional.ofNullable(entry.getTelephone()).ifPresent(val->contactPointBuilder.add(SCHEMA.TELEPHONE,val));
		Model model = contactPointBuilder.build();
		return model;
	}

	private Model createLocationModel(IRI geometryIRI, IRI addressIRI, IRI locationIRI) {
		ModelBuilder locationBuilder = createModelBuilder().subject(locationIRI).add(RDF.TYPE, DCTERMS.LOCATION);
		locationBuilder.add(LOCN.GEOMETRY, geometryIRI);
		locationBuilder.add(LOCN.ADDRESS, addressIRI);
		Model model = locationBuilder.build();
		return model;
	}

	private Model createAddressModel(org.openapitools.client.model.Entry entry, IRI addressIRI) {
		ModelBuilder addressBuilder = createModelBuilder().subject(addressIRI).add(RDF.TYPE, LOCN.ADDRESS);
		Optional.ofNullable(entry.getZip()).ifPresent(val->addressBuilder.add(LOCN.POST_CODE,val));
		Optional.ofNullable(entry.getCountry()).ifPresent(val->addressBuilder.add(LOCN.ADMIN_UNIT_L1,val));
		Optional.ofNullable(entry.getState()).ifPresent(val->addressBuilder.add(LOCN.ADMIN_UNIT_L2,val));
		Optional.ofNullable(entry.getCity()).ifPresent(val->addressBuilder.add(LOCN.POST_NAME,val));
		Optional.ofNullable(entry.getStreet()).ifPresent(val->addressBuilder.add(LOCN.ADDRESS_AREA,val));
		Model model = addressBuilder.build();
		return model;
	}

	private Model createGeometryModel(org.openapitools.client.model.Entry entry, IRI geometryIRI) {
		ModelBuilder geometryBuilder = createModelBuilder().subject(geometryIRI).add(RDF.TYPE, LOCN.GEOMETRY);
		Optional.ofNullable(entry.getLat()).ifPresent(val->geometryBuilder.add(SCHEMA.LATITUDE, val));
		Optional.ofNullable(entry.getLng()).ifPresent(val->geometryBuilder.add(SCHEMA.LONGITUDE, val));
		Model model = geometryBuilder.build();
		return model;
	}

	private ModelBuilder createModelBuilder() {
		ModelBuilder builder = new ModelBuilder();
		namespaces.forEach(builder::setNamespace);
		return builder;
	}
	
	private static IRI createRelatedIri(String entryIri, String subEntity) {
		return iri(BASE_URL + entryIri + "/" + subEntity);
	}
}
